import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import ApplicationRouter from "./router/ApplicationRouter";
import { Switch, Router as ReactRouter } from "react-router-dom";
import history from "./history";
import routes from "./router/routes";
import "bootstrap/dist/css/bootstrap.min.css";
const App = (props) => {
  return (
    <>
      <div className="app">
        <ReactRouter history={history}>
          <Switch>
            {routes.map((route, index) => {
              return <ApplicationRouter key={index} {...route} />;
            })}
          </Switch>
        </ReactRouter>
      </div>
    </>
  );
};

export default App;
