import React from "react";
import "./style.css";

const ProductDetailsComponent = ({ productDetails }) => {
  return (
    <>
      <h1 className="text-center component-title">Product Details</h1>
      {productDetails && (
        <div className="card-deck row product-details">
          <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 product">
            <img
              className="card-img-top product-image"
              src={window.location.origin + productDetails.productImage}
              alt={productDetails.productTitle}
            />
          </div>

          <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
            <div className="product-details-wrapper">
              <h3 className="product-title">{productDetails.productTitle}</h3>
              <p className="card-text">{productDetails.description}</p>
              <p className="card-text">{productDetails.detailedDescription}</p>

              <p className="text-primary">$ {productDetails.price}</p>
              {productDetails.availability === "sold" && (
                <div>
                  <button
                    className="btn btn-danger rounded-pill"
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                    }}
                  >
                    Sold
                  </button>
                </div>
              )}
              {productDetails.availability === "available" && (
                <div>
                  <button
                    className="btn btn-primary rounded-pill"
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                    }}
                  >
                    Available
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ProductDetailsComponent;
