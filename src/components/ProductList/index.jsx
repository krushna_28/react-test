import React, { useState } from "react";
import { getValue, productCategory } from "../../utilities/app";
import "./style.css";
import { Link, useHistory } from "react-router-dom";

const ProductListComponent = ({
  productList,
  resetProductListFilter,
  getProductListWithFilter,
}) => {
  const history = useHistory();
  const [isFilterActive, setIsFilterActive] = useState([]);

  const onCategoryFilterClick = (event) => {
    if (getValue(event, "event.target.id") === "all") {
      if (!isFilterActive.includes(getValue(event, "event.target.id"))) {
        resetProductListFilter();
        setIsFilterActive([getValue(event, "event.target.id")]);
      } else {
        setIsFilterActive([]);
      }
    } else {
      let filterData = isFilterActive;
      if (filterData.includes("all")) {
        filterData.splice(filterData.indexOf("all"), 1);
      }
      if (filterData.includes(getValue(event, "event.target.id"))) {
        filterData.splice(
          filterData.indexOf(getValue(event, "event.target.id")),
          1
        );
      } else {
        filterData.push(getValue(event, "event.target.id"));
      }
      setIsFilterActive(filterData);
      getProductListWithFilter(filterData);
    }
  };
  
  const onProductDetailsClick = (product) => {
    history.push("/product-detail/" + product._id);
  };
  const renderProductList = () => {
    return (
      productList.length > 0 &&
      Array.isArray(productList) &&
      productList.map((product, index) => {
        return (
          <div className="col-xs-12 col-sm-6 col-md-3 list-wrapper" key={index}>
            <div
              className="card"
              onClick={() => onProductDetailsClick(product)}
            >
              <div className="view overlay">
                <img
                  className="card-img-top"
                  src={window.location.origin + product.productImage}
                  alt={product.productTitle}
                />
              </div>

              <div className="card-body">
                <h4 className="card-title">{product.productTitle}</h4>
                <p className="card-text">{product.description}</p>

                <p className="text-primary">$ {product.price}</p>
                {product.availability === "sold" && (
                  <button
                    className="btn btn-danger rounded-pill"
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                    }}
                  >
                    Sold
                  </button>
                )}
                {product.availability === "available" && (
                  <button
                    className="btn btn-primary rounded-pill"
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                    }}
                  >
                    Available
                  </button>
                )}
              </div>
            </div>
          </div>
        );
      })
    );
  };
  return (
    <>
      <h1 className="text-center component-title">Product List</h1>
      <div className="category-filter">
        <div className="btn-group" role="group" aria-label="Basic example">
          {Array.isArray(Object.keys(productCategory)) &&
            Object.keys(productCategory).map((categoryDetail) => {
              return (
                <button
                  type="button"
                  id={categoryDetail}
                  key={productCategory[categoryDetail]}
                  className={
                    isFilterActive.includes(categoryDetail)
                      ? "btn filter-btn btn-primary"
                      : "btn filter-btn btn-outline-primary"
                  }
                  onClick={onCategoryFilterClick}
                >
                  {productCategory[categoryDetail]}
                </button>
              );
            })}
        </div>
      </div>
      <div className="card-deck row">{renderProductList()}</div>
    </>
  );
};

export default ProductListComponent;
