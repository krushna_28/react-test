import React, { useEffect, useState } from "react";
import ProductListComponent from "../../components/ProductList";
import { getProductList } from "../../services/apiService";
import { getValue } from "../../utilities/app";

const ProductListContainer = () => {
  const [allProductList, setAllProductList] = useState([]);
  const [productList, setProductList] = useState([]);

  useEffect(() => {
    getAllProductList();
  }, []);

  const getAllProductList = () => {
    getProductList().then((response) => {
      setProductList(getValue(response, "response.data.data.productList"));
      setAllProductList(getValue(response, "response.data.data.productList"));
    });
  };

  const getProductListWithFilter = (filterData) => {
    if (filterData.length > 0) {
      let newProductList = [];
      allProductList.forEach((product) => {
        let intersection = product.category.filter((x) =>
          filterData.includes(x)
        );
        if (intersection.length > 0) {
          newProductList.push(product);
        }
      });
      setProductList(newProductList);
    } else {
      setProductList(allProductList);
    }
  };

  const resetProductListFilter = () => {
    setProductList(allProductList);
  };

  return (
    <>
      <ProductListComponent
        productList={productList}
        resetProductListFilter={resetProductListFilter}
        getProductListWithFilter={getProductListWithFilter}
      />
    </>
  );
};

export default ProductListContainer;
