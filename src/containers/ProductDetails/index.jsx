import React, { useEffect, useState } from "react";
import ProductDetailsComponent from "../../components/ProductDetails";
import PageNotFoundComponent from "../../components/PageNotFound";
import { getProductList } from "../../services/apiService";
import { getValue } from "../../utilities/app";

const ProductDetailsContainer = ({ computedMatch }) => {
  const [productDetails, setProductDetails] = useState(false);
  const getProductDetail = () => {
    getProductList().then((response) => {
      let productList = getValue(
        response,
        "response.data.data.productList"
      ).filter((product) => computedMatch.params.productId === product._id);
      setProductDetails(productList[0]);
    });
  };

  useEffect(() => {
    getProductDetail();
  }, []);

  return (
    <>
      {productDetails ? (
        <ProductDetailsComponent productDetails={productDetails} />
      ) : (
        <PageNotFoundComponent />
      )}
    </>
  );
};

export default ProductDetailsContainer;
