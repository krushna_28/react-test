import { request } from "../utilities/request";
export function getProductList(requestBody) {
  let url = window.location.origin + "/stubs/getProductList.json";
  return request(url, "get", {}, requestBody)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
}

const apiService = {
  getProductList,
};

export default apiService;
