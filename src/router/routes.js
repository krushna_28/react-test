import ProductList from "../containers/ProductList";
import ProductDetails from "../containers/ProductDetails";

const routes = [
  {
    path: ["/", "/product-list"],
    exact: true,
    component: ProductList,
  },
  {
    path: ["/product-detail/:productId"],
    exact: true,
    component: ProductDetails,
  },
];

export default routes;
