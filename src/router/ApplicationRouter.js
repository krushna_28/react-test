import React from "react";
import { Route } from "react-router-dom";

const ApplicationRouter = ({ ...route }) => {
  return (
    <Route
      path={route.path}
      render={(props) => (
        <route.component
          {...props}
          routes={route.routes}
          computedMatch={route.computedMatch}
        />
      )}
    />
  );
};
export default ApplicationRouter;
