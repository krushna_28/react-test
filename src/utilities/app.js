export const getValue = (obj, expression) => {
  try {
    return expression.split(".").reduce((o, x, i) => {
      if (i === 0) {
        return o;
      }
      return typeof o === "undefined" || o === null ? undefined : o[x];
    }, obj);
  } catch (e) {
    console.error("getValue => " + e);
    return undefined;
  }
};

export const productCategory = {
  all: "All",
  indian: "Indian",
  italian: "Italian",
  chinese: "Chinese",
  veg: "Veg",
  nonVeg: "Non-veg",
};
