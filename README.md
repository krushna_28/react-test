# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### Features used in this project

- User "Develop" branch for latest code

Technology REACT.JS

- Custom Hooks
- Reuse form component
- Multi-step forms
- Firebase Database
- OTP service
- Email Service

EmailJS

- Automated Email notifications

Note\*\*\*
Please add mobile no in formate of this "+91" + " "(space)+"9925241345"

example "+91 9925241345"

flow :

1. For Admin Portal

- /admin-login (For Admin login) (Integrated firebase for runtime OTP and Captcha)
- /appointments (For All appointment) (API integrated with fire store)

2. For Customer Portal

- /signup (for signUp ) (Integrated with firebase)
- /login (for login ) (Integrated firebase for runtime OTP and Captcha)
- /home or / (for homepage) (Static home page with book appointment button)
- /services (for services) (Static service page with book appointment button)
- /my-appointment (for all my appointment)(connected with fire store to get all my appointment details)(currently filter is not dynamically working but for particular number filter is working)
- /booking (for booking)(multi step booking work for all field validation and after booking it will send confirmation email to user)

- email part is archived via emailJS

addition feature - logout functionality

almost completed feature :

- we want to add document in booking for 90% completed for pdf upload and review via "react-pdf"
- pdf download functionality for generating pdf and download pdf via "jspdf"
